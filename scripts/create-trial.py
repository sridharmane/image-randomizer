from operator import indexOf
import os
import shutil
import numpy as np

from utils import createDirsIfNotExists, generateCode, isImageFile, printFilesMapToCSV

###################################
# SETTINGS
###################################

IMAGES_SRC_DIR = './images-etched/'
TRIALS_DIR = './trials/'
FILES_MAP_FILE_NAME = 'map.csv'
TRIAL_CODE_LENGTH = 6

###################################
# SETTINGS END
###################################

files_map = {}

def create_trial():

    # Read Source Directory and iterate
    files = os.listdir(IMAGES_SRC_DIR)
    for file in files:
        filePath = os.path.join(IMAGES_SRC_DIR, file)
        if os.path.isfile(filePath) and isImageFile(filePath):
            files_map[file] = None
    fileNames = list(files_map.keys())
    print('Shuffled files:')
    print('From: ', fileNames)
    np.random.shuffle(fileNames)
    print('To:   ', fileNames)
    files_map.fromkeys(files)
    for file in fileNames:
        ext = os.path.basename(file).split('.')[1]
        files_map[file] = str(indexOf(fileNames, file)+1)+'.'+ext
    print(files_map)


    trialID = generateCode(TRIAL_CODE_LENGTH)
    dirDest = os.path.join(TRIALS_DIR, trialID);
    print(dirDest)
    createDirsIfNotExists(dirDest)
    for file in fileNames:
        fileSrc = os.path.join(IMAGES_SRC_DIR, file)
        fileDest = os.path.join(dirDest, files_map[file])
        shutil.copy(fileSrc, fileDest)
    csvFilePath = os.path.join(dirDest, FILES_MAP_FILE_NAME);
    printFilesMapToCSV(csvFilePath, files_map)

create_trial()

