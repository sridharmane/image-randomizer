# IMAGE RANDOMIZER
A utility to randomize a set of images to be used in settings such as a research trial. The premise being, given a set of images, create another set of these images with randomized order.

## Setup
### Prerequisites
* Python - Install the latest Python 3 (for windows: https://www.python.org/downloads/windows/)
### Prepare
1. Clone/download this repo (say to `image-randomizer`)
2. Go to `image-randomizer`
3. Run `setup.bat` to:
   1. Install all dependencies
   2. Create folder `image-randomizer/images`
   3. Create folder `image-randomizer/images-etched`
   4. Create folder `image-randomizer/trials`
4. Copy all the images that need to be randomized into `image-randomizer/images`

## Etch Images
To add a watermark text to all images, run `etch-images.bat`. Running this script will:
1. Creates a new folder `image-randomizer/images-etched`
2. For each image in `image-randomizer/images`,
   1. Generates a unique code (3 characters from within A-Z)
   2. Sets this unique code as the file name
   3. Sets this unique code as watermark text on the image
3. Creates a mappiing file at `image-randomizer/images-etched/map.csv` with all the mappings of the before`(image-randomizer/images)` and after`(image-randomizer/images-etched)` file names

## Create Trial
Running `create-trial.bat` dows the following:
1. Generates a unique code`{trialCode}` for the trial. (6 characters from within A-Z)
2. Creates the folder at `image-randomizer/trials/{trialCode}`
3. Reads the `image-randomizer/images-etched` folder and randomizes the list of the images
4. This new list of files is renamed as a sequence starting from 1 `(1.jpeg, 2.jpeg etc)`
5. Images are copied into `image-randomizer/trials/{trialCode}` with this new sequence.
6. Creates a `image-randomizer/trials/{trialCode}/map.csv` with all the mappings of the before`(image-randomizer/images-etched)` and after`(image-randomizer/trials/{trialCode}/)`file names