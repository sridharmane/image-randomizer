import enum
import os
import random

ALLOWED_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
_generatedCodes = {}

class TextPosition(enum.Enum):
    TOP_LEFT = 0
    TOP_RIGHT = 1
    BOTTOM_LEFT = 2
    BOTTOM_RIGHT = 3


def generateCode(length=3):
    code = ""
    while len(code) < length:
        code += str(random.choice(ALLOWED_CHARS))

    inserted = False
    while not inserted:
        if not _generatedCodes.get(code):
            _generatedCodes[code] = 1
            inserted = True

    return code


def printFilesMapToCSV(path, fileMap):
    csvFile = os.open(path,os.O_RDWR|os.O_CREAT)
    for key in fileMap.keys():
        os.write(csvFile, bytes(key+','+fileMap[key], 'utf-8'))
        os.write(csvFile, bytes('\n', 'utf-8'))
    os.close(csvFile)
    print('File mapping saved to: '+ path)

def isImageFile(filePath):
    return filePath.endswith('.jpeg') or filePath.endswith('.jpg') or filePath.endswith('.png')

def createDirsIfNotExists(dirPath):
    try:
        os.makedirs(dirPath)
        print('Created Dir', dirPath)
    except OSError as error:
        print(error) 