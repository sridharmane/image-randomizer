from dataclasses import asdict
import os
# Importing the PIL library
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from utils import TextPosition, generateCode, printFilesMapToCSV, createDirsIfNotExists

###################################
# SETTINGS
###################################

# Image Sources
IMAGES_SRC_DIR = './images/'
IMAGES_DIST_DIR = './images-etched/'

# Font Settings
FONT_PATH = 'fonts/Roboto-Bold.ttf'
FONT_SIZE = 32
FONT_COLOR = (255, 255, 255) # White
font = ImageFont.truetype(FONT_PATH, FONT_SIZE)

# Position settings to place place text on image
TEXT_POSITION_PADDING_X = 12
TEXT_POSITION_PADDING_Y = 12
TEXT_POSITION = TextPosition.TOP_LEFT

# Length of the code used to etch the image and rename the file
FILE_NAME_LENGTH = 3

###################################
# SETTINGS END
###################################

file_map = {}

def etch_image(filePath, text):
    # Open an Image
    img = Image.open(filePath)
    (width, height) = img.size

    textX = textY = 0
    if TEXT_POSITION == TextPosition.TOP_LEFT:
        textX = 0       + TEXT_POSITION_PADDING_X
        textY = 0       + TEXT_POSITION_PADDING_Y
    elif TEXT_POSITION == TextPosition.TOP_RIGHT:
        textX = width   - TEXT_POSITION_PADDING_X
        textY = 0       + TEXT_POSITION_PADDING_Y
    elif TEXT_POSITION == TextPosition.BOTTOM_LEFT:
        textX = 0       + TEXT_POSITION_PADDING_X
        textY = height  - TEXT_POSITION_PADDING_Y
    elif TEXT_POSITION == TextPosition.BOTTOM_RIGHT:
        textX = width   - TEXT_POSITION_PADDING_X
        textY = height  - TEXT_POSITION_PADDING_Y

    drawer = ImageDraw.Draw(img)
    drawer.text((textX, textY), text, fill=FONT_COLOR, font=font)

    fileName = os.path.basename(filePath)
    fileExt = os.path.splitext(fileName)[1]

    file_map[fileName] = code + fileExt


    destinationPath = os.path.join(IMAGES_DIST_DIR, file_map[fileName])
    img.save(destinationPath)
    print('Etched: ', destinationPath)

# Read Source Directory and iterate
files = os.listdir(IMAGES_SRC_DIR)
createDirsIfNotExists(IMAGES_DIST_DIR)

for file in files:
    filePath = os.path.join(IMAGES_SRC_DIR, file)
    if os.path.isfile(filePath) and (filePath.endswith('.jpeg') or filePath.endswith('.jpg') or filePath.endswith('.png')):
        code = generateCode(FILE_NAME_LENGTH)
        etch_image(filePath, code)



# Save the Map file into a CSV
csvFilePath =  os.path.join(IMAGES_DIST_DIR, 'map.csv')

csvFile = os.open(csvFilePath,os.O_RDWR|os.O_CREAT)

for key in file_map.keys():
    os.write(csvFile, bytes(key+','+file_map[key], 'utf-8'))
    os.write(csvFile, bytes('\n', 'utf-8'))
os.close(csvFile)
print(file_map)

printFilesMapToCSV(csvFilePath, file_map);


