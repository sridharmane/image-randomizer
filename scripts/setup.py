import sys
import subprocess

from utils import createDirsIfNotExists

###################################
# SETTINGS
###################################

# Image Sources
IMAGES_SRC_DIR = './images/'
IMAGES_DIST_DIR = './images-etched/'
TRIALS_DIR = './trials/'

###################################
# SETTINGS END
###################################

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'Pillow', 'numpy'])

# process output with an API in the subprocess module:
reqs = subprocess.check_output([sys.executable, '-m', 'pip',
'freeze'])
installed_packages = [r.decode().split('==')[0] for r in reqs.split()]

print('Installed packages', installed_packages)

createDirsIfNotExists(IMAGES_SRC_DIR)
createDirsIfNotExists(IMAGES_DIST_DIR)
createDirsIfNotExists(TRIALS_DIR)
